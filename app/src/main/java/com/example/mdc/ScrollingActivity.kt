package com.example.mdc

import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.URLUtil
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.mdc.databinding.ActivityScrollingBinding
import com.google.android.material.bottomappbar.BottomAppBar

class ScrollingActivity : AppCompatActivity() {

    private lateinit var binding: ActivityScrollingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_scrolling)
        binding = ActivityScrollingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /*findViewById<FloatingActionButton>(R.id.fab).setOnClickListener {
            if (findViewById<BottomAppBar>(R.id.bottom_app_bar).fabAlignmentMode == BottomAppBar.FAB_ALIGNMENT_MODE_CENTER) {
                findViewById<BottomAppBar>(R.id.bottom_app_bar).fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_END
            } else {
                findViewById<BottomAppBar>(R.id.bottom_app_bar).fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_CENTER
            }
        }*/
        binding.fab.setOnClickListener {
            if (binding.bottomAppBar.fabAlignmentMode == BottomAppBar.FAB_ALIGNMENT_MODE_CENTER) {
                binding.bottomAppBar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_END
            } else {
                binding.bottomAppBar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_CENTER
            }
        }

        binding.bottomAppBar.setNavigationOnClickListener {
            Snackbar.make(binding.root, R.string.message_action_succes, Snackbar.LENGTH_LONG)
                .setAnchorView(binding.fab)
                .show()
        }

        binding.contentScrolling.btnSkip.setOnClickListener {
            binding.contentScrolling.cvAd.visibility = View.GONE
        }

        binding.contentScrolling.btnBuy.setOnClickListener {
            Snackbar.make(it, R.string.card_buying, Snackbar.LENGTH_LONG)
                .setAnchorView(binding.fab)
                .setAction(R.string.card_to_go) {
                    Toast.makeText(this, R.string.card_historial, Toast.LENGTH_LONG).show()
                }
                .show()
        }

        loadImage(imageView = binding.contentScrolling.imgCover)

        binding.contentScrolling.cbEnablePass.setOnClickListener {
            binding.contentScrolling.tilPassword.isEnabled = !binding.contentScrolling.tilPassword.isEnabled
        }

        binding.contentScrolling.etUrl.onFocusChangeListener = View.OnFocusChangeListener { _, focused ->
            var errorStr: String? = null
            val url = binding.contentScrolling.etUrl.text.toString()

            if (!focused) {
                /*if (url.isEmpty()){
                    binding.contentScrolling.tilUrl.error = getString(R.string.card_required)
                } else if (URLUtil.isValidUrl(url)){
                    loadImage(url, binding.contentScrolling.imgCover)
                } else {
                    errorStr = getString(R.string.card_invalid_url)
                }*/
                when {
                    url.isEmpty() -> binding.contentScrolling.tilUrl.error = getString(R.string.card_required)
                    URLUtil.isValidUrl(url) -> loadImage(url, binding.contentScrolling.imgCover)
                    else -> errorStr = getString(R.string.card_invalid_url)
                }
            }

            binding.contentScrolling.tilUrl.error = errorStr
        }

        binding.contentScrolling.toggleButton.addOnButtonCheckedListener { _, checkedId, _ ->
            binding.contentScrolling.root.setBackgroundColor(
                when(checkedId) {
                    R.id.btnRed -> Color.RED
                    R.id.btnBlue -> Color.BLUE
                    R.id.btnGreen -> Color.GREEN
                    else -> Color.GREEN
                }
            )
        }

        binding.contentScrolling.swFab.setOnCheckedChangeListener { button, isChecked ->
            if (isChecked) {
                button.text = getString(R.string.card_hide_fab)
                binding.fab.show()
            } else {
                button.text = getString(R.string.card_show_fab)
                binding.fab.hide()
            }
        }

        binding.contentScrolling.sldVol.addOnChangeListener { slider, value, fromUser ->
            binding.contentScrolling.tvSubtitle.text = "Volumen: $value"
        }

        binding.contentScrolling.cpEmail.setOnCheckedChangeListener { chip, isChecked ->
            if (isChecked) {
                Toast.makeText(this,"${chip.text}", Toast.LENGTH_LONG).show()
            }
        }

        binding.contentScrolling.cpEmail.setOnCloseIconClickListener {
            binding.contentScrolling.cpEmail.visibility = View.GONE
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_scrolling, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun loadImage(url: String = "https://i.pinimg.com/564x/7e/9e/6d/7e9e6dadc5cf9a8a37a8657f5f90f914.jpg", imageView: ImageView) {
        Glide.with(this)
            .load(url)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .into(imageView)
    }
}